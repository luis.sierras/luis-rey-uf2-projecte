package cat.itb.uf2.luisrey.dam.m3.projecte.ui;

import cat.itb.uf2.luisrey.dam.m3.projecte.data.League;

import java.util.Scanner;

public class MainMenu {
    public static void main(String[] args) {
        League league = new League();
        Scanner scanner = new Scanner(System.in);
        boolean continuar = displayMenu(scanner, league);
        while (continuar) {
            continuar = displayMenu(scanner, league);
        }
    }

    static boolean displayMenu(Scanner scanner, League league) {
        System.out.println("-------MENU-------\n" +
                "1. Datos\n" +
                "2. Estadisticas\n" +
                "0. Salir");
        int opcion = scanner.nextInt();

        switch (opcion) {
            case 1:
                MenuDades.displyDatos(scanner, league);
                return true;
            case 2:
                MenuEstadistiques.displayEstadisticas(scanner, league);
                return true;
            case 0:
                break;
        }
        return false;
    }
}
