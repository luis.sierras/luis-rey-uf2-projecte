package cat.itb.uf2.luisrey.dam.m3.projecte.ui;

import cat.itb.uf2.luisrey.dam.m3.projecte.data.League;
import cat.itb.uf2.luisrey.dam.m3.projecte.data.Match;

import java.util.Scanner;

public class MenuDades {
    static void displyDatos(Scanner scanner, League league) {
        System.out.println("-------DATOS-------\n" +
                "1. Añadir equipo\n" +
                "2. Añadir partido\n" +
                "3. Lista equipos\n" +
                "4. Lista partidos\n" +
                "5. Modificar equipo\n" +
                "6. Modificar partida\n" +
                "0. Salir");
        int opcion = scanner.nextInt();
        switch (opcion) {
            case 1:
                League.addTeam(scanner, league);
                break;
            case 2:
                League.addMatch(scanner, league);
                break;
            case 3:
                League.printTeams(league);
                break;
            case 4:
                League.printMatch(league);
                break;
            case 5:
                League.ModifyTeam(scanner, league);
                break;
            case 6:
                ModifyMatch(scanner, league);
                break;
            case 0:
                break;
        }
    }

    private static void ModifyMatch(Scanner scanner, League league) {
        System.out.println("Introduce el ID del partido que deseas modificar");
        int id = scanner.nextInt();
        for (Match match : league.matches) {
            if (id == match.idMatch) {
                System.out.println("Deseas modificar los datos del equipo:\n" +
                        "1. Local\n" +
                        "2. Visitante\n" +
                        "3. Ambos\n" +
                        "0. Salir\n");
                int opcion = scanner.nextInt();
                switch (opcion) {
                    case 1:
                        League.ModifyMatchLocal(scanner, league, match);
                        break;
                    case 2:
                        League.ModifyMatchVisitor(scanner, league, match);
                        break;
                    case 3:
                        League.ModifyMatchLocal(scanner, league, match);
                        League.ModifyMatchVisitor(scanner, league, match);
                        break;
                    case 0:
                        break;
                }
            }
        }
    }
}